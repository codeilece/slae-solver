import numpy as np

class RSPMatrix:
    #Row sparse matrix
    value: np.ndarray
    cols: np.ndarray
    pointer: np.ndarray
    shape: list

    def __init__(self, value, cols, pointer, shape):
        self.value = np.array(value)
        self.cols = np.array(cols, dtype=np.int32)
        self.pointer = np.array(pointer, dtype=np.int32)
        self.shape = list(shape)

    
    def parse(matrix:np.ndarray):
        values_and_cols = np.array(list((filter(lambda x: x[1] != 0, enumerate(matrix.flatten())))))
        if len(values_and_cols) == 0:
            value = np.array([])
            cols = np.array([])
            pointer = np.zeros(matrix.shape[0] + 1)
        else:
            value = values_and_cols[:, 1]
            cols = values_and_cols[:, 0] % matrix.shape[1]
            pointer = np.cumsum([0] + [len(list(filter(lambda el: el != 0, array))) for array in matrix])
        shape = matrix.shape
        return RSPMatrix(value, cols, pointer, shape)

    def __str__(self):
        return str(self.to_dens())

    def __repr__(self):
        res = "RowSparseMatrix(\n"
        res += f"Value: {self.value.__repr__()}; \n"
        res += f"Columns: {self.cols.__repr__()}; \n"
        res += f"Pointers: {self.pointer.__repr__()})"
        return res

    def to_dens(self):
        matrix = np.zeros(self.shape)
        for inds in enumerate(zip(self.pointer[:-1], self.pointer[1:])):
            for i in np.arange(*inds[1]):
                matrix[inds[0]][self.cols[i]] = self.value[i]
        return matrix
    
    def T(self):
        nvecs = [[] for _ in range(self.shape[1])]
        ncols = [[] for _ in range(self.shape[1])]
        for inds in enumerate(zip(self.pointer[:-1], self.pointer[1:])):
            for i in np.arange(*inds[1]):
                nvecs[self.cols[i]].append(self.value[i])
                ncols[self.cols[i]].append(inds[0])
        value = np.array(sum(nvecs, []))
        cols = np.array(sum(ncols, []))
        pointer = np.cumsum([0] + [len(array) for array in nvecs])
        shape = (self.shape[1], self.shape[0])
        return RSPMatrix(value, cols, pointer, shape)
    
    def __add__(self, b):
        if self.shape != b.shape:
            raise RuntimeError(f"Difference dimensions! {self.shape} and {b.shape}.")
        value = []
        cols = []
        pointer = [0]
        count = 0
        for inds in enumerate(zip(zip(self.pointer[:-1], self.pointer[1:]), zip(b.pointer[:-1], b.pointer[1:]))):
            j1 = inds[1][0][0]
            j2 = inds[1][1][0]
            while j1 < inds[1][0][1] and j2 < inds[1][1][1]:
                if self.cols[j1] == b.cols[j2]:
                    res = self.value[j1] + b.value[j2]
                    if res != 0:
                        value.append(res)
                        cols.append(self.cols[j1])
                        count += 1
                    j1 += 1
                    j2 += 1
                elif self.cols[j1] < b.cols[j2]:
                    value.append(self.value[j1])
                    cols.append(self.cols[j1])
                    count += 1
                    j1 += 1
                else:
                    value.append(b.value[j2])
                    cols.append(b.cols[j2])
                    count += 1
                    j2 += 1
            for k in np.arange(j1, inds[1][0][1]):
                value.append(self.value[k])
                cols.append(self.cols[k])
                count += 1
            for k in np.arange(j2, inds[1][1][1]):
                value.append(b.value[k])
                cols.append(b.cols[k])
                count += 1
            pointer.append(count)
        return RSPMatrix(value, cols, pointer, self.shape)

    def __rmul__(self, b):
        if b == 0:
            value = np.array([])
            cols = np.array([])
            pointer = np.zeros(self.shape[0] + 1)
            shape = self.shape
        else: 
            value = b * self.value
            cols = self.cols
            pointer = self.pointer
            shape = self.shape
        return RSPMatrix(value, cols, pointer, shape)
    
    def __sub__(self, b):
        return self + (-1) * b

    def __mul__(self, b):
        if isinstance(b, (int, float)):
            return b * self
        else:
            return NotImplemented

    def __truediv__(self, b):
        if isinstance(b, (int, float)):
            return (1 / b) * self
        else:
            return NotImplemented

    def __matmul__(self, b):
        if isinstance(b, RSPMatrix):
            if self.shape[1] != b.shape[0]:
                raise RuntimeError(f"Does not match dim. {self.shape} and {b.shape}")
            bt = b.T()
            value = []
            cols = []
            pointer = [0]
            shape = [self.shape[0], b.shape[1]]
            count = 0
            for inds1 in enumerate(zip(self.pointer[:-1], self.pointer[1:])):
                for inds2 in enumerate(zip(bt.pointer[:-1], bt.pointer[1:])):
                    res = 0
                    j1 = inds1[1][0]
                    j2 = inds2[1][0]
                    while j1 < inds1[1][1] and j2 < inds2[1][1]:
                        if self.cols[j1] == bt.cols[j2]:
                            res += self.value[j1] * bt.value[j2]
                            j1 += 1
                            j2 += 1
                        elif self.cols[j1] < bt.cols[j2]:
                            j1 += 1
                        else:
                            j2 += 1
                    if res != 0:
                        value.append(res)
                        cols.append(inds2[0])
                        count += 1
                pointer.append(count)
            return RSPMatrix(value, cols, pointer, shape)
        else:
            return NotImplemented