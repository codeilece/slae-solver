import numpy as np
from lab1.sparse_matrix import RSPMatrix

def parse_arrays_it(input):
    class parse_arrays:
        def __init__(self, input:str):
            self.input = input.strip()[1:-1].replace(" ", "")
        def __iter__(self):
            self.start = 0
            self.end = 0
            return self
        def __next__(self):
            self.start = self.input.find("[", self.end)
            self.end = self.input.find("]", self.start + 1)
            if self.start == -1:
                raise StopIteration
            return self.input[self.start + 1:self.end]
    return np.array([el.split(",") for el in iter(parse_arrays(input))], dtype=float)

with open("matrix/input.txt") as f:
    lines = f.readlines()
matrix1 = parse_arrays_it(lines[0])
matrix2 = parse_arrays_it(lines[1])
output = ""

#TEST1 PARSE MATRIX:
rmatrix1 = RSPMatrix.parse(matrix1)
rmatrix2 = RSPMatrix.parse(matrix2)
output += f"TEST 1 REPRESENTATION\n {rmatrix1.__repr__()}\n\n"

#TEST2 TRANSPOSE MATRIX
output += f"TEST 2 TRANSPOSE MATRIX\n {rmatrix1}\n{rmatrix1.T()}\n{rmatrix1.T().__repr__()}\n\n"

#TEST3 SUM MATRIX
output += f"TEST 3 SUM MATRIX\n {rmatrix1} +\n {rmatrix2.T()} =\n {rmatrix1 + rmatrix2.T()}\n {(rmatrix1 + rmatrix2.T()).__repr__()}\n\n"

#TEST4 MULT WITH CONSTANT
output += f"TEST4 MULT WITH CONSTANT\n 3 * \n{rmatrix1} =\n {3 * rmatrix1}\n{(3 * rmatrix1).__repr__()}\n"

#TEST5 MULT WITH MATRIX
output += f"TEST5 MULT WITH MATRIX\n {rmatrix1} @\n {rmatrix2} =\n {rmatrix1 @ rmatrix2}\n{(rmatrix1 @ rmatrix2).__repr__()}\n\n"

with open("matrix/output.txt", "w") as f:
    f.write(output)
